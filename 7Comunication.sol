pragma solidity ^0.4.0;

contract CallerContract {
    CalledContract toBeCalled = CalledContract(0x368ad6328db21f181cace7787a3eb323bc76576d);
    //CalledContract toBeCalled = new CalledContract();
    
    function getNumber() constant returns(uint){
        return toBeCalled.getNumber();
    }
}

contract CalledContract {
    uint number = 300;
    function getNumber() constant returns(uint){
        return number;
    }
}