pragma solidity ^0.4.0;

contract Calculator {
    //Math math = new Math();
    Math math = Math(0xcd021ffd464dc40614c4dd7e7826b3d91a21ab12);
    
    function twoPlusFour() constant returns(int){
        return math.add(2,4);
    }
    
    
    function twoTimesFour() constant returns(int){
        return math.multiply(2,4);
    }
}



contract Math {
    function add(int a, int b) returns(int){
        return a + b;
    }
    
    function multiply(int a, int b) returns(int){
        return a * b;
    }
}