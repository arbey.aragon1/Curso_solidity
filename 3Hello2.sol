pragma solidity ^0.4.0;

contract HelloWorldContract {
    string word = "Hola mundo";
    address owner;
    
    function HelloWorldContract(){
        owner = msg.sender;
    }
    
    modifier onlyOwner() {
        if(owner != msg.sender){
            throw;
        } else {
            _;
        }
    }
    
    function getWord() constant returns(string){
        return word;
    }
    
    function setWord(string newWord) onlyOwner returns(string){
        word = newWord;
        return "Si eres el creador del contrato";
    }
}