pragma solidity ^0.4.0;

contract Savings{
    address owner;
    
    event UpdateStatus(string msg);
    event UpdateUserStatus(string _msg, address user, uint amount);
    
    
    function Savings(){
        owner = msg.sender;
    }
        
    modifier onlyOwner {
        if(msg.sender != owner){
            throw;
        } else {
            _;
        }
    }
    
    function kill() onlyOwner {
        suicide(owner);
    }
    
    function depositFunds() payable {
        UpdateUserStatus('User widthdraw some money', msg.sender, msg.value);
    }
    
    function widthdrawnFunds(uint amount) onlyOwner{
        if(owner.send(amount)){
            UpdateStatus('User widthdraw some money');
        }
    }
    
    
    function getFunds() onlyOwner constant returns(uint){
        return this.balance;
    }
}